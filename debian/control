Source: r-cran-terra
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp,
               libgdal-dev
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-terra
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-terra.git
Homepage: https://cran.r-project.org/package=terra
Rules-Requires-Root: no

Package: r-cran-terra
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R spatial data analysis
 Methods for spatial data analysis with raster and vector data. Raster
 methods allow for low-level data manipulation as well as high-level
 global, local, zonal, and focal computation. The predict and interpolate
 methods facilitate the use of regression type (interpolation, machine
 learning) models for spatial prediction, including with satellite remote
 sensing data. Processing of very large files is supported. See the
 manual and tutorials on <https://rspatial.org/terra/> to get started.
 'terra' is very similar to the 'raster' package; but 'terra' can do
 more, is easier to use, and it is faster.
